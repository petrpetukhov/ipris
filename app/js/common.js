$(function() {


	$(document).ready(function () {
		$('.main-navigation .--with-sublist').each(function (i, el) {
			$(this).children('a, span').after('<i class="sublist-arrow"></i>');
		});
	});

	// аккордион в мобильном меню
	$('.nav-menu').on('click', '.sublist-arrow', function () { 
		if($(window).width() < 1199 && $(window).width() >= 768) {
			if( $(this).closest('li').hasClass('toggled') ) {
				$(this).closest('li').removeClass('toggled');
				$(this).parent().find('.sublist').hide();
			} else {
				$(this).closest('ul').find('.sublist').hide();
				$(this).closest('ul').find('li').removeClass('toggled');
				$(this).closest('li').addClass('toggled');
				$(this).parent().children('.sublist').show();
			}
		} else if ($(window).width() < 767) {
			if( $(this).closest('li').hasClass('toggled') ) {
				$(this).closest('li').removeClass('toggled');
				$(this).parent().find('.sublist').slideUp(200);
			} else {
				$(this).closest('ul').find('.sublist').slideUp(200);
				$(this).closest('ul').find('li').removeClass('toggled');
				$(this).closest('li').addClass('toggled');
				$(this).parent().children('.sublist').slideDown(200);
			}
		}
	});

	
	$('.hero-slider').slick({
		dots: true,
		prevArrow: '<button type="button" class="slick-prev"></button>',
		nextArrow: '<button type="button" class="slick-next"></button>'
	});
	
	$('.main-catalog__slider').slick({
		speed: 300,
		slidesToShow: 5,
		prevArrow: '<button type="button" class="slick-prev"></button>',
		nextArrow: '<button type="button" class="slick-next"></button>',
		responsive: [
			{
			  breakpoint: 1199,
			  settings: {
				slidesToShow: 4,
			  }
			},{
				breakpoint: 991,
				settings: {
				  slidesToShow: 3,
				}
			},{
				breakpoint: 767,
				settings: {
				  slidesToShow: 2,
				}
			},{
				breakpoint: 400,
				settings: {
				  slidesToShow: 1,
				}
			},
		]
	});
	
	$('.category-catalog__slider').slick({
		speed: 300,
		slidesToShow: 6,
		adaptiveHeight: true,
		prevArrow: '<button type="button" class="slick-prev"></button>',
		nextArrow: '<button type="button" class="slick-next"></button>',
		responsive: [
			{
			  breakpoint: 1199,
			  settings: {
				slidesToShow: 4,
			  }
			},{
				breakpoint: 991,
				settings: {
				  slidesToShow: 3,
				}
			},{
				breakpoint: 767,
				settings: {
				  slidesToShow: 2,
				}
			},{
				breakpoint: 400,
				settings: {
				  slidesToShow: 1,
				}
			},
		]
	});


	//Scroll button 
	$(window).scroll(function () {
		if ($(this).scrollTop() > 100 && ($(window).width() > 1200) ) {
			$('.scroll-up').fadeIn();
		} else {
			$('.scroll-up').fadeOut();
		}
	});

	$('.scroll-up').click(function () {
		$('body, html').animate({scrollTop: 0}, 500);
			return false;
	});	



	$('.storefront-sorting .filter-button').click(function (e) { 

		if ( !$(this).hasClass('active') ) {
			$(this).addClass('active');
			$('.products').removeClass('columns-4');
			$('.products').addClass('columns-3');
			$('.widget-area').show();
			if( $(window).width() < 1200 ) $('body').addClass('noscroll');
		} else {
			$(this).removeClass('active');
			$('.products').removeClass('columns-3');
			$('.products').addClass('columns-4');
			$('.widget-area').hide();
		}
		
	});

	$('.widget-title').click(function (e) { 
		$(this).toggleClass('active');
		$(this).closest('.woof_container').find('.woof_container_inner').slideToggle(200);
	});
	
	$(document).ready(function () {
		if( $('.widget-area').length ) {
			$('.widget-area').append('<span class="widget-area-close"></span>');
		}
	});

	$('.widget-area').on('click', '.widget-area-close', function (e) {
		$('.widget-area').hide();
		$('body').removeClass('noscroll');
	}); 


	
	
	// Галерея на странице доставки
	if( $('.shipment__item').length ) {
		
		var ftitle;
		$('.shipment__img a').click(function (e) { 
			ftitle = $(this).closest('.shipment').find('.ftitle').text();
		});
		$(".shipment [data-fancybox]").fancybox({
			buttons: [
				"close"
			],
			infobar: false,
			animationEffect: false,
			baseTpl:
				'<div class="fancybox-container" role="dialog" tabindex="-1">' +
				'<div class="fancybox-bg"></div>' +
				'<div class="fancybox-inner">' +
				'<div class="fancybox-toolbar">{{buttons}}</div>' +
				'<div class="fancybox-navigation">{{arrows}}</div>' +
				'<h3 class="fancybox-title"></h3>' +
				'<div class="fancybox-stage"></div>' +
				'<div class="fancybox-caption"><div class="fancybox-caption__body"></div></div>' +
				'</div>' +
				'</div>',
			btnTpl: {
				arrowLeft:
				  '<button data-fancybox-prev class="fancybox-button fancybox-button--arrow_left">' +
				  "</button>",
			
				arrowRight:
				  '<button data-fancybox-next class="fancybox-button fancybox-button--arrow_right">' +
				  "</button>",
	
			},
			clickSlide: false,
			beforeLoad: function( instance, slide ) {
				$('.fancybox-title').text(ftitle);
			}
		});
		
		$('.shipment__item').each(function (i, el) {
			$(this).find('.shipment__img a').attr('data-caption', $(this).find('.shipment__txt').html());
			
		});
	}


	// quantity plus minus
	$(document).on('click', '.quantity-input .minus', function(){
		var $_inp = $(this).parent().find('input');
		$_inp.val( parseInt( $_inp.val() ) - 1 );
		$_inp.trigger('propertychange');
		return false;
	});
	$(document).on('click', '.quantity-input .plus', function(){
		var $_inp = $(this).parent().find('input');
		$_inp.val( parseInt( $_inp.val() ) + 1 );
		$_inp.trigger('propertychange');
		return false;
	});
	$('.quantity-input input').bind('input propertychange', function () {
		var $this = $(this);
		$this.val( $this.val().replace(/[^0-9]/gim, '') );
		if ( $this.val().length == 0 || parseInt( $this.val() ) <= 0 )
		$this.val(1);
	});



	


});